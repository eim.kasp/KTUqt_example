#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "studentas.h"

#include <QMainWindow>
#include <QWidget>

class QPushButton;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    int m_counter;
    string pvz;

    ~MainWindow();

private slots:
    void slotButtonClicked(bool checked);

private:
    Ui::MainWindow *ui;
    QPushButton *m_button;

};

#endif // MAINWINDOW_H
