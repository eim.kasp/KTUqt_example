#ifndef STUDENTAS_H
#define STUDENTAS_H
#include <QDebug>
#include <string>



using namespace std;
class studentas
{
public:
    //studentas(string vard);
    static const int CMaxPaž = 10;
    string duotiVarda();
    studentas(string pv, string gr, int n, int P[]);
    studentas() {}
    ~studentas();
private:
    string vardas;
    string pavv; // pavardė ir vardas
    string grp; // grupė
    int Paž[CMaxPaž]; // pažymių masyvas
    int np; // pažymių kiekis
    double vid; // vidurkis
public:

     void DėtiStud(string pv, string gr, int n, int P[]);
    /** Grąžina pavardę ir vardą */
     string ImtiPavv() { return pavv; }

     string ImtiGrp() { return grp; }
    /** Grąžina pažymių skaičių */
     int ImtiNp() { return np; }
    /** Grąžina pažymių vidurkį */
     double ImtiVid() { return vid; }
    /** Grąžina i-ąjį pažymį
     @param i – pažymio numeris */
     int ImtiPaž(int i) { return Paž[i]; }

};

#endif // STUDENTAS_H
