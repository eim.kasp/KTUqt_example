#-------------------------------------------------
#
# Project created by QtCreator 2015-02-04T09:16:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = P1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    studentas.cpp \
    functions.cpp

HEADERS  += mainwindow.h \
    studentas.h

FORMS    += mainwindow.ui

DISTFILES += \
    duom.txt
