#include "mainwindow.h"
#include "studentas.h"
#include "functions.cpp"

#include "ui_mainwindow.h"
#include <QPushButton>
#include <iostream>
#include <fstream>


const char CDuom[] = "/Users/eimantas/P1/duom.txt";

void nuskaitymas(const char fv[], studentas  A[]) {

    ifstream fd(fv);
    studentas stud;
    string eil, // informacinė eilutė
    pavv, // studento pavardė ir vardas
    grupe; // grupė, kurioje mokosi studentas
//    getline(fd, eil); // praleidžia pirmą (informacinę) eilutę

    while (!fd.eof()) {
        getline(fd, pavv, ','); // skaito pavardę ir vardą
     //   qDebug() << pavv.c_str();

        fd >> ws; // praleidžia visus tarpus iki grupės pavadinimo
        getline(fd, grupe, ' '); // skaito grupę
//        qDebug() << grupe.c_str();

    // Pažymių skaitymas iki eilutės pabaigos
    int Paž[studentas::CMaxPaž];
    int kiekPaž = 0;
    int i = 0;
    while (fd.peek() != '\n' && !fd.eof()) {
        fd >> Paž[kiekPaž++];
    }

    fd.ignore();
    A[i].DėtiStud(pavv, grupe, kiekPaž, Paž);
    }
    fd.close();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // Set size of the window
    // setFixedSize(100, 50);

    // Create and position the button
    m_button = new QPushButton("Hello World", this);
    m_button->setGeometry(100, 80, 180, 130);
    m_button->setCheckable(true);

    qDebug() << "programos pradzia";
    qDebug() << mano_testas();


    studentas *inform = new studentas[5]; // dinaminis studentų masyvas

    nuskaitymas(CDuom, inform);

    qDebug() << inform[2].duotiVarda().c_str();
    //pvz=  stud1.ImtiPavv();
    pvz = inform[1].duotiVarda();


    connect(m_button, SIGNAL(clicked(bool)), this, SLOT(slotButtonClicked(bool)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::slotButtonClicked(bool checked)
{
    if (checked) {
        m_button->setText("Checked");
    } else {
        QString s = QString::number(m_counter);

        m_button->setText(pvz.c_str());
    }

    m_counter++;
}




